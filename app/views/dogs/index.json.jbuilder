json.array!(@dogs) do |dog|
  json.extract! dog, :id, :name, :image
  json.url dog_url(dog, format: :json)
end
